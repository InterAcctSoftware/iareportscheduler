﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("IAReportScheduler")>
<Assembly: AssemblyDescription("InterAcct Report Scheduler so reports can be run every x hour/day etc.")>
<Assembly: AssemblyCompany("InterAcct Software Pty Ltd")>
<Assembly: AssemblyProduct("IAReportScheduler")>
<Assembly: AssemblyCopyright("Copyright ©  2017-2024")>
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("975e4df5-f745-44f3-8867-9cb34fe97a48")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")>
<Assembly: AssemblyFileVersion("24.11.15.0")>
