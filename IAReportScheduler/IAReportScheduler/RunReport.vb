﻿Imports System
Imports System.IO
Imports System.Text
Module RunReport

    'This Function is to run the Super Report form under the Schedule Directory under the location of the Program file.
    ' PWR 16.03.2017

    Public Structure Schreport
        Dim SystemTitle As String
        Dim UserCode As String
        Dim Password As String
        Dim Created_DateTime As Date
        Dim ReportName As String
        Dim ReportDescription As String
        Dim NextScheduled_DateTime As Date
        Dim Period As String
        Dim PeriodValue As Double
        Dim DataDirectory As String
        Dim ExecutableDirectory As String
        Dim CommonFileLocation As String
        Dim SettingsFileLocation As String
        Dim RunFromTime As TimeSpan
        Dim RunTillTime As TimeSpan
        Dim M5CommonFile As String
        Dim Active As Boolean
        Dim IncludeFolderinSchedule As Boolean
    End Structure

    Public IASchReport As New List(Of Schreport)

    Public gVersionNumber As String = Application.ProductVersion
    Public ApplicationTitle As String = String.Format("{0} - Version : {1} ", My.Application.Info.ProductName, gVersionNumber)

    Dim gDataDirectory As String = vbNullString
    Dim gSystemTitle As String = vbNullString

    Public Function GetFileContents(ByVal FullPath As String, _
                                 Optional ByRef ErrInfo As String = vbNullString) As Integer

        Dim objReader As StreamReader
        Try
            objReader = New StreamReader(FullPath)
            While Not objReader.EndOfStream
                Dim strContents As String = objReader.ReadLine()
                If InStr(LCase(strContents), "datadirectory=") Then
                    gDataDirectory = Mid(strContents, (InStr(strContents, "=") + 1))
                ElseIf InStr(LCase(strContents), "title=") Then
                    gSystemTitle = Mid(strContents, (InStr(strContents, "=") + 1))
                End If
            End While
            objReader.Close()
            Return 1
        Catch Ex As Exception
            ErrInfo = Ex.Message
            Return 0
        End Try
    End Function

    Private Sub SetupColumnHeadings()

        IASchReport.Clear()



        With frmMain.DataGridView1
            .Rows.Clear()
            If .ColumnCount = 0 Then

                '.DataSource = IASchReport

                .SelectionMode = DataGridViewSelectionMode.FullRowSelect
                .MultiSelect = False
                '.Dock = DockStyle.Fill

                Dim objSystemTitleName As New DataGridViewTextBoxColumn
                With objSystemTitleName
                    .Name = "System Title"
                    .Width = 200
                End With
                .Columns.Add(objSystemTitleName)

                Dim objReportName As New DataGridViewTextBoxColumn
                With objReportName
                    .Name = "ReportName"
                    .Width = 90
                End With
                .Columns.Add(objReportName)

                Dim objReportDescription As New DataGridViewTextBoxColumn
                With objReportDescription
                    .Name = "ReportDescription"
                    .Width = 380
                End With
                .Columns.Add(objReportDescription)

                Dim objReportNextDate As New DataGridViewTextBoxColumn
                With objReportNextDate
                    .Name = "Next Date"
                    .Width = 90
                End With
                .Columns.Add(objReportNextDate)

                Dim objPeriodFromTime As New DataGridViewTextBoxColumn
                With objPeriodFromTime
                    .Name = "From Time"
                    .Width = 60
                End With
                .Columns.Add(objPeriodFromTime)

                Dim objPeriodTillTime As New DataGridViewTextBoxColumn
                With objPeriodTillTime
                    .Name = "Till Time"
                    .Width = 60
                End With
                .Columns.Add(objPeriodTillTime)

                Dim objPeriod As New DataGridViewComboBoxColumn
                With objPeriod
                    .HeaderText = "Period"
                    .Name = "Period"
                    .Width = 90
                    .MaxDropDownItems = 4
                    .Items.Add("SECOND")
                    .Items.Add("MINUTE")
                    .Items.Add("HOUR")
                    .Items.Add("DAY")
                    .Items.Add("WEEK")
                    .Items.Add("MONTH")
                    .Items.Add("YEAR")
                End With
                .Columns.Add(objPeriod)

                Dim objPeriodValue As New DataGridViewTextBoxColumn
                With objPeriodValue
                    .Name = "Period Value"
                    .Width = 50
                End With
                .Columns.Add(objPeriodValue)

                Dim objActive As New DataGridViewCheckBoxColumn
                With objActive
                    .Name = "Active?"
                    .Width = 40
                End With
                .Columns.Add(objActive)

                Dim objHide As New DataGridViewCheckBoxColumn
                With objHide
                    .Name = "Hide?"
                    .Width = 40
                    .Visible = False
                End With
                .Columns.Add(objHide)

            End If


        End With
    End Sub

    Public Sub CheckDirectory(ByRef Dir_Name As String)
        If Dir_Name.Length > 0 Then
            If Not Mid(Dir_Name, Dir_Name.Length, 1) = "\" Then
                Dir_Name &= "\"
            End If
        End If
    End Sub

    Private Function GetScheduledFiles() As Boolean

        Dim ScheduleDirectory As String = vbNullString
        Dim CurrentReportIndex As Integer = 0

        GetScheduledFiles = False

        If Len(gDataDirectory) = 0 Then
            Exit Function
        End If

        If Mid(gDataDirectory, Len(gDataDirectory), 1) <> "\" Then
            gDataDirectory &= "\"
        End If

        ScheduleDirectory = gDataDirectory & "Scheduled\"

        If Directory.Exists(ScheduleDirectory) Then

            Dim CurrentDateTime As Date = Now
            ' Make a reference to a directory.
            Dim di As New DirectoryInfo(ScheduleDirectory)
            ' Get a reference to each file in that directory.
            Dim fiArr As FileInfo() = di.GetFiles("*.csv")
            ' Display the names of the files.
            Dim fri As FileInfo
            For Each fri In fiArr
                Using MyReader As New Microsoft.VisualBasic.
                              FileIO.TextFieldParser(fri.FullName)
                    MyReader.TextFieldType = FileIO.FieldType.Delimited
                    MyReader.SetDelimiters(",")
                    Dim currentRow As String()
                    While Not MyReader.EndOfData
                        Try
                            currentRow = MyReader.ReadFields()
                            CurrentReportIndex += 1
                            Dim ReportSchedule As New Schreport()
                            Dim currentField As String
                            Dim fCounter As Integer = 0
                            With ReportSchedule
                                For Each currentField In currentRow
                                    fCounter += 1


                                    Select Case fCounter
                                        Case 1 : .DataDirectory = gDataDirectory
                                        Case 2 : .UserCode = currentField

                                            Dim UserAlreadyExists As Boolean = False
                                            Dim xItems As Object
                                            For Each xItems In frmMain.ChkListBoxUsers.Items
                                                If InStr(UCase(xItems.ToString), UCase(currentField)) <> 0 Then
                                                    UserAlreadyExists = True
                                                    Exit For
                                                End If
                                            Next

                                            If UserAlreadyExists = False Then
                                                frmMain.ChkListBoxUsers.Items.Add(currentField, True)
                                            End If

                                        Case 3 : .Created_DateTime = DateTime.ParseExact(currentField, "yyyyMMddHHmmss", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                                        Case 4 : .ReportName = currentField
                                        Case 5 : .NextScheduled_DateTime = DateTime.ParseExact(currentField, "yyyyMMddHHmmss", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                                        Case 6 : .Period = UCase$(currentField)
                                        Case 7 : .PeriodValue = Format(currentField)
                                        Case 8 : .RunFromTime = TimeSpan.Parse(currentField)
                                        Case 9 : .RunTillTime = TimeSpan.Parse(currentField)
                                        Case 10 : .ExecutableDirectory = currentField
                                        Case 11 : .ReportDescription = currentField
                                        Case 12 : .M5CommonFile = currentField
                                        Case 13 : .Active = (UCase(currentField) = "TRUE")
                                    End Select
                                Next

                                'PWR 15.11.2024 Now Check if the Next Report Date is in the Future, If not change the data
                                While .NextScheduled_DateTime < CurrentDateTime
                                    SetNewRunDateTime_NextTime(ReportSchedule, .NextScheduled_DateTime)
                                End While

                                CheckDirectory(.DataDirectory)

                                .IncludeFolderinSchedule = True

                                If Len(.ReportDescription) = 0 Then
                                    .ReportDescription = .ReportName
                                End If

                                .SettingsFileLocation = fri.FullName
                                .SystemTitle = gSystemTitle
                                .CommonFileLocation = Mid(fri.FullName, 1, Len(fri.FullName) - 3) & "ISF"

                                frmMain.DataGridView1.Rows.Add(.SystemTitle, .ReportName, .ReportDescription, Format(.NextScheduled_DateTime, "HH:mm dd/MM/yy"), .RunFromTime.ToString, .RunTillTime.ToString, .Period, Format(.PeriodValue), .Active, False)
                            End With

                            GetScheduledFiles = True
                            IASchReport.Add(ReportSchedule)

                        Catch ex As Microsoft.VisualBasic.
                                    FileIO.MalformedLineException
                            MsgBox("Line " & ex.Message &
                            "is not valid and will be skipped.")
                        End Try
                    End While
                End Using
            Next fri
        End If

    End Function


    Public Sub ReadCSVFiles()


        Dim CurrentDirectory As String = vbNullString
        Dim ExecDirectory As String = vbNullString
        Dim FoundScheduledItems As Boolean = False



#If DEBUG Then
        ExecDirectory = "C:\IA\"
        'ExecDirectory = "C:\Program Files (x86)\ClearView"
#Else
        ExecDirectory = Application.StartupPath
#End If

        If Mid(ExecDirectory, Len(ExecDirectory), 1) <> "\" Then
            ExecDirectory &= "\"
        End If

        Dim SingleINIFile As String = vbNullString
        Dim Commands() As String = Environment.GetCommandLineArgs()

        If Commands.Length > 1 Then
            If InStr(Commands(1), ".ini") > 0 Then
                SingleINIFile = Trim(Commands(1))
            End If
        End If


        With frmMain.ChkListBoxSystems
            .Items.Clear()

            SetupColumnHeadings()

            'PWR 23.03.2017 Look at the Local Directory Files
            gDataDirectory = ExecDirectory
            gSystemTitle = ExecDirectory
            If GetScheduledFiles() = True Then
                .Items.Add("User Scheduled - " & ExecDirectory, True)
            End If
        End With

        ' Make a reference to a directory.
        Dim inidi As New DirectoryInfo(ExecDirectory)
        ' Get a reference to each file in that directory.
        Dim inifiArr As FileInfo() = inidi.GetFiles("*.ini")
        ' Display the names of the files.
        Dim inifri As FileInfo
        If inifiArr.Count > 0 Then          'Check if it found any INI Files
            For Each inifri In inifiArr
                Using iniMyReader As New Microsoft.VisualBasic.
                              FileIO.TextFieldParser(inifri.FullName)

                    If ((SingleINIFile = vbNullString) Or (LCase(SingleINIFile) = LCase(inifri.Name))) Then

                        gDataDirectory = vbNullString
                        gSystemTitle = vbNullString

                        If File.Exists(String.Format("{0}{1}", ExecDirectory, inifri.Name)) Then
                            GetFileContents(String.Format("{0}{1}", ExecDirectory, inifri.Name), "")

                            'Check if that Directory is already being checked... if the INI
                            Dim DBAlreadyExists As Boolean = False
                            Dim xItems As Object
                            For Each xItems In frmMain.ChkListBoxSystems.Items
                                If InStr(UCase(xItems.ToString), UCase(gDataDirectory)) <> 0 Then
                                    DBAlreadyExists = True
                                    Exit For
                                End If
                            Next

                            If DBAlreadyExists = False Then
                                If GetScheduledFiles() = True Then
                                    frmMain.ChkListBoxSystems.Items.Add(String.Format("{0} - {1}", gSystemTitle, gDataDirectory), True)
                                End If
                            End If

                        End If

                    End If

                End Using
            Next inifri

        End If



    End Sub

    Private Sub Run_Report_Now(ByVal ReportSchedule As schreport)
        Dim proc As New System.Diagnostics.Process()
        Dim ReportProgram As String = ReportSchedule.ExecutableDirectory & "\cvsuper.exe"
        Dim ReportSettingsString As String = _
         "," & ChrW(34) & ReportSchedule.CommonFileLocation & ChrW(34) & ChrW(44) & ReportSchedule.DataDirectory & ChrW(44) & _
        Trim$(ReportSchedule.ReportName) & ",,," & ReportSchedule.UserCode & ChrW(44) & ChrW(44) & ReportSchedule.ExecutableDirectory & ChrW(44) & _
        "ISAM" & ChrW(44) & ChrW(44) & ChrW(44)
        proc = Process.Start(ReportProgram, ReportSettingsString)

        frmMain.lblLastRunReport.Text = "Last Report : " & ReportSchedule.DataDirectory & "Scheduled\" & ReportSchedule.ReportName & " at " & Format(Now, "HH:mm dd/MM/yyy")

    End Sub

    Public Sub SaveScheduleReport(ByRef ReportSchedule As schreport, ByVal ReportIndex As Integer)

        Dim NewStringforCSV As String = vbNullString
        Dim sp As String = ChrW(34) & "," & ChrW(34)

        With ReportSchedule

            NewStringforCSV = ChrW(34) & .DataDirectory & sp & _
                                    .UserCode & sp & _
                                    Format(.Created_DateTime, "yyyyMMddHHmmss") & sp & _
                                    .ReportName & sp & _
                                    Format(.NextScheduled_DateTime, "yyyyMMddHHmmss") & sp & _
                                    .Period & sp & _
                                    Format(.PeriodValue) & sp & _
                                    .RunFromTime.ToString & sp & _
                                    .RunTillTime.ToString & sp & _
                                    .ExecutableDirectory & sp & _
                                    .ReportDescription & sp & _
                                    .M5CommonFile & sp & _
                                    .Active.ToString & ChrW(34)



        End With



        Dim path As String = ReportSchedule.SettingsFileLocation

        If File.Exists(path) Then
            ' Create a file to write to. 
            Using sw As StreamWriter = File.CreateText(path)
                sw.WriteLine(NewStringforCSV)
            End Using
        End If

    End Sub



    Private Sub SetNewRunDateTime_NextTime(ByRef ReportSchedule As Schreport, ByRef AddFromDateTime As Date)

        With ReportSchedule
            Select Case UCase(.Period)
                Case "SECONDS", "SECOND" : .NextScheduled_DateTime = AddFromDateTime.AddSeconds(.PeriodValue)
                Case "MINUTE", "MINUTES" : .NextScheduled_DateTime = AddFromDateTime.AddMinutes(.PeriodValue)
                Case "HOUR", "HOURS" : .NextScheduled_DateTime = AddFromDateTime.AddHours(.PeriodValue)
                Case "DAY", "DAYS" : .NextScheduled_DateTime = AddFromDateTime.AddDays(.PeriodValue)
                Case "WEEK", "WEEKS" : .NextScheduled_DateTime = AddFromDateTime.AddDays((.PeriodValue * 7))
                Case "FORTNIGHT" : .NextScheduled_DateTime = AddFromDateTime.AddDays((.PeriodValue * 14))
                Case "MONTH", "MONTHS" : .NextScheduled_DateTime = AddFromDateTime.AddMonths(.PeriodValue)
                Case "QUARTER", "QUARTERS" : .NextScheduled_DateTime = AddFromDateTime.AddMonths((.PeriodValue * 3))
                Case "HALFYEAR", "HALF" : .NextScheduled_DateTime = AddFromDateTime.AddMonths((.PeriodValue * 6))
                Case "YEAR", "YEARS" : .NextScheduled_DateTime = AddFromDateTime.AddYears(.PeriodValue)
            End Select

        End With
    End Sub

    Private Sub SetNewRunDateTime(ByRef ReportSchedule As schreport, ByRef ReportIndex As Integer)

        Dim CurrentTimeDate As Date = Now
        SetNewRunDateTime_NextTime(ReportSchedule, CurrentTimeDate)

        SaveScheduleReport(ReportSchedule, ReportIndex)  'Now Save to CSV File

        frmMain.DataGridView1.Rows(ReportIndex).Cells(3).Value = Format(ReportSchedule.NextScheduled_DateTime, "HH:mm dd/MM/yy")    'Starting at Zero

    End Sub

    Public Sub CheckSchedule()
        'Now run the functions
        Dim CountofReports As Integer = 0
        Dim CountofReportsNotRunning As Integer = 0
        Dim ReportsNotRun As String = "These reports have not been run : " & vbNewLine

        Dim CurrentDateTime As Date = Now
        If IASchReport.Count > 0 Then
            For Each ReportSchedule In IASchReport
                With ReportSchedule
                    'Nighttime (.RunFromTime > .RunTillTime) 'Not Between the times
                    'Daytime (.RunFromTime < .RunTillTime)    'Between the Times.
                    If (.RunFromTime = .RunTillTime) Or _
                       ((.RunFromTime > .RunTillTime) And ((CurrentDateTime.TimeOfDay > .RunFromTime) Or (CurrentDateTime.TimeOfDay < .RunTillTime))) Or _
                       ((.RunFromTime < .RunTillTime) And ((CurrentDateTime.TimeOfDay > .RunFromTime) And (CurrentDateTime.TimeOfDay < .RunTillTime))) Then

                        If ReportSchedule.NextScheduled_DateTime < CurrentDateTime Then
                            CountofReports += 1
                            ReportsNotRun = ReportsNotRun & .ReportDescription & " was scheduled at " & Format(.NextScheduled_DateTime, "HH:mm dd/MM/yy") & vbNewLine
                        Else
                            CountofReportsNotRunning += 1
                        End If
                    Else
                        CountofReportsNotRunning += 1
                    End If
                End With

            Next
        End If

        If CountofReports > 0 Then
            ReportsNotRun = ReportsNotRun & vbNewLine & "Do you want to run these reports now?"
            Dim mResult As Integer = MsgBox(ReportsNotRun, MsgBoxStyle.YesNo, "IA Report Scheduler - Reports not Run")
            If mResult = MsgBoxResult.Yes Then
                RunSchedule()
            End If
        Else
            MsgBox("No Reports are currently required to be run, but there are " & Format(CountofReportsNotRunning) & " scheduled to run", MsgBoxStyle.OkOnly, "IA Report Scheduler - Nothing to Run")

        End If

    End Sub

    Public Sub RunSchedule()
        'Now run the functions
        Dim CurrentDateTime As Date = Now
        Dim xActiveReports As Integer = 0
        Dim NextDateTime As Date = Now
        Dim NextReport As String = vbNullString

        If IASchReport.Count > 0 Then
            For x As Integer = 0 To IASchReport.Count - 1
                With IASchReport(x)
                    'Nighttime (.RunFromTime > .RunTillTime) 'Not Between the times
                    'Daytime (.RunFromTime < .RunTillTime)    'Between the Times.
                    If .Active = True And .IncludeFolderinSchedule = True Then
                        If (.RunFromTime = .RunTillTime) Or _
                           ((.RunFromTime > .RunTillTime) And ((CurrentDateTime.TimeOfDay > .RunFromTime) Or (CurrentDateTime.TimeOfDay < .RunTillTime))) Or _
                           ((.RunFromTime < .RunTillTime) And ((CurrentDateTime.TimeOfDay > .RunFromTime) And (CurrentDateTime.TimeOfDay < .RunTillTime))) Then

                            If IASchReport(x).NextScheduled_DateTime < CurrentDateTime Then
                                Call Run_Report_Now(IASchReport(x))
                                Call SetNewRunDateTime(IASchReport(x), x)
                            Else
                                If NextReport = vbNullString Or NextDateTime > IASchReport(x).NextScheduled_DateTime Then
                                    NextDateTime = IASchReport(x).NextScheduled_DateTime
                                    NextReport = IASchReport(x).DataDirectory & "Scheduled\" & IASchReport(x).ReportName
                                End If
                            End If
                        Else
                            If NextReport = vbNullString Or NextDateTime > IASchReport(x).NextScheduled_DateTime Then
                                NextDateTime = IASchReport(x).NextScheduled_DateTime
                                NextReport = IASchReport(x).DataDirectory & "Scheduled\" & IASchReport(x).ReportName
                            End If
                        End If
                        xActiveReports += 1
                    End If
                End With

            Next

            If Len(NextReport) > 0 Then
                frmMain.lblNextScheduled.Text = "Next Report to Run : " & NextReport & " at " & Format(NextDateTime, "HH:mm:ss dd/MM/yyyy")
            Else
                frmMain.lblNextScheduled.Text = "Next Report to Run : No Active Reports to schedule."
            End If

            If xActiveReports = 1 Then
                frmMain.lblLastChecked.Text = "Last checked : " & Format(CurrentDateTime, "HH:mm:ss dd/MM/yyyy") & " : Nothing to run : " & xActiveReports.ToString & " active report scheduled"
            Else
                frmMain.lblLastChecked.Text = "Last checked : " & Format(CurrentDateTime, "HH:mm:ss dd/MM/yyyy") & " : Nothing to run : " & xActiveReports.ToString & " active reports scheduled"
            End If


        Else
            frmMain.lblLastChecked.Text = "Last checked : " & Format(CurrentDateTime, "HH:mm:ss dd/MM/yyyy") & " : No reports scheduled"
        End If

    End Sub


End Module
