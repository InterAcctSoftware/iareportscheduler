﻿Public Class FrmMain

    'Dim gCount As Integer = 0       'Counter to ReRead the CSV Files.

    Private Sub ToggleButtonsActive(ActiveControl As Boolean)
        ToolStripButtonPause.Enabled = ActiveControl
        ToolStripButtonPlay.Enabled = Not ActiveControl
    End Sub

    Private Sub RestartProgram()
        ReadCSVFiles()
    End Sub


    Private Sub DeleteSelectedReport()

        Dim x As Integer = Me.DataGridView1.CurrentCell.RowIndex
        Dim DeleteOccured As Boolean = False


        With IASchReport(x)

            Dim mResult As Integer = MsgBox(String.Format("Are you sure you want to delete {1} from the schedule.{0}{0}You can later re-add the report from your InterAcct System.{0}{0}Ready to Delete?", Environment.NewLine, .ReportName), MsgBoxStyle.YesNo, "IA Report Scheduler - Delete Report")
            If mResult = MsgBoxResult.Yes Then
                If System.IO.File.Exists(.CommonFileLocation) = True Then
                    System.IO.File.Delete(.CommonFileLocation)
                    'MsgBox("File Deleted")
                    DeleteOccured = True

                End If

                If System.IO.File.Exists(.SettingsFileLocation) = True Then
                    System.IO.File.Delete(.SettingsFileLocation)
                    'MsgBox("File Deleted")
                    DeleteOccured = True
                End If
            End If

        End With

        If DeleteOccured Then
            Call ReadCSVFiles()
        End If

    End Sub


    Private Sub StartFunctions()
        ToggleButtonsActive(True)
        Timer1.Enabled = True
    End Sub

    Private Sub PauseFunctions()
        ToggleButtonsActive(False)
        Timer1.Enabled = False
    End Sub


    Private Sub BtnRunNow_Click(sender As System.Object, e As System.EventArgs)
        'CreateSchedule()
        StartFunctions()
    End Sub

    Public Sub CreateSchedule()
        Call ReadCSVFiles()
        Call CheckSchedule()
    End Sub

    Private Sub FrmMain_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim arg As String() = System.Environment.GetCommandLineArgs()

        Me.Text = ApplicationTitle
        ReadCSVFiles()
        Me.DataGridView1.Focus()
        If arg.Length > 2 Then      'PWR 06.05.2020 Function to Run at the start.. Starts at a Zero Reference
            If arg(2) = "-run" Then
                ToggleButtonsActive(True)
                Timer1.Enabled = True
            End If
        End If

    End Sub

    Private Function IsANonHeaderLinkCell(ByVal cellEvent As _
    DataGridViewCellEventArgs) As Boolean

        If TypeOf DataGridView1.Columns(cellEvent.ColumnIndex) _
            Is DataGridViewLinkColumn _
            AndAlso Not cellEvent.RowIndex = -1 Then _
            Return True Else Return False

    End Function

    Private Function IsANonHeaderButtonCell(ByVal cellEvent As _
        DataGridViewCellEventArgs) As Boolean

        If TypeOf DataGridView1.Columns(cellEvent.ColumnIndex) _
            Is DataGridViewButtonColumn _
            AndAlso Not cellEvent.RowIndex = -1 Then _
            Return True Else Return (False)

    End Function

    Private Function IsActiveQuestionCell(ByVal cellEvent As _
    DataGridViewCellEventArgs) As Boolean

        If TypeOf DataGridView1.Columns(cellEvent.ColumnIndex) _
            Is DataGridViewCheckBoxColumn _
            AndAlso Not cellEvent.RowIndex = -1 _
            AndAlso cellEvent.ColumnIndex = 8 Then _
            Return True Else Return (False)

    End Function

    Private Function IsPeriodValueCell(ByVal cellEvent As _
DataGridViewCellEventArgs) As Boolean

        If TypeOf DataGridView1.Columns(cellEvent.ColumnIndex) _
            Is DataGridViewTextBoxColumn _
            AndAlso Not cellEvent.RowIndex = -1 _
            AndAlso cellEvent.ColumnIndex = 7 Then _
            Return True Else Return (False)

    End Function

    Private Function IsPeriodTypeCell(ByVal cellEvent As _
DataGridViewCellEventArgs) As Boolean

        If TypeOf DataGridView1.Columns(cellEvent.ColumnIndex) _
            Is DataGridViewComboBoxColumn _
            AndAlso Not cellEvent.RowIndex = -1 _
            AndAlso cellEvent.ColumnIndex = 6 Then _
            Return True Else Return (False)

    End Function

    Private Function IsTillTimeCell(ByVal cellEvent As _
DataGridViewCellEventArgs) As Boolean

        If TypeOf DataGridView1.Columns(cellEvent.ColumnIndex) _
            Is DataGridViewTextBoxColumn _
            AndAlso Not cellEvent.RowIndex = -1 _
            AndAlso cellEvent.ColumnIndex = 5 Then _
            Return True Else Return (False)

    End Function

    Private Function IsFromTimeCell(ByVal cellEvent As _
DataGridViewCellEventArgs) As Boolean

        If TypeOf DataGridView1.Columns(cellEvent.ColumnIndex) _
            Is DataGridViewTextBoxColumn _
            AndAlso Not cellEvent.RowIndex = -1 _
            AndAlso cellEvent.ColumnIndex = 4 Then _
            Return True Else Return (False)

    End Function




    Private Sub DataGridView1_CellContentClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

        If e.RowIndex < 0 Then
            Exit Sub
        End If

        Dim ReportSchedule As Schreport = IASchReport(e.RowIndex)
        If IsActiveQuestionCell(e) Then
            ReportSchedule.Active = Not ReportSchedule.Active       'Flip Around
            SaveScheduleReport(ReportSchedule, e.RowIndex)
            'gCount = 58     'Allow for the refresh to happen soon.
            'ElseIf IsPeriodValueCell(e) Then

            'SaveScheduleReport(ReportSchedule, e.RowIndex)
        ElseIf IsPeriodTypeCell(e) Then
            ReportSchedule.PeriodValue = DataGridView1.Rows(e.RowIndex) _
            .Cells("Period").Value.ToString()
            SaveScheduleReport(ReportSchedule, e.RowIndex)


            'ElseIf IsANonHeaderLinkCell(e) Then

            'ElseIf IsANonHeaderButtonCell(e) Then

        End If



    End Sub


    Private Sub Timer1_Tick(sender As Object, e As System.EventArgs) Handles Timer1.Tick
        'If gCount = 60 Then       'Every Minute reread the CSV Files
        'Call ReadCSVFiles()
        'gCount = 0
        'Else
        'gCount += 1
        'End If

        'CheckSchedule()        'Check if it needs to be run
        RunSchedule()           'Just Run it
    End Sub



    Private Sub ChkListBoxSystems_ItemCheck(sender As Object, e As System.Windows.Forms.ItemCheckEventArgs) Handles ChkListBoxSystems.ItemCheck
        'Dim messageBoxVB As New System.Text.StringBuilder()
        'messageBoxVB.AppendFormat("{0} = {1}", "Index", e.Index)
        'messageBoxVB.AppendLine()
        'messageBoxVB.AppendFormat("{0} = {1}", "NewValue", e.NewValue)
        'messageBoxVB.AppendLine()
        'messageBoxVB.AppendFormat("{0} = {1}", "CurrentValue", e.CurrentValue)
        'messageBoxVB.AppendLine()
        'MessageBox.Show(messageBoxVB.ToString(), "ItemCheck Event")

        Dim CheckDataDirectory As String = Me.ChkListBoxSystems.Items(e.Index).ToString
        Dim Pos As Integer = InStrRev(CheckDataDirectory, " - ")

        CheckDataDirectory = Mid(CheckDataDirectory, Pos + 3, 80)

        For x As Integer = 0 To IASchReport.Count - 1
            If UCase(IASchReport(x).DataDirectory) = UCase(CheckDataDirectory) Then
                Dim dgvRow As DataGridViewRow = Me.DataGridView1.Rows(x)
                Dim xReportStructure As Schreport = RunReport.IASchReport(x)
                If e.NewValue = CheckState.Checked Then

                    'Me.DataGridView1.Rows(x).Frozen = False

                    'If dgvRow.Cells("Active?").Value <> True Then
                    ' dgvRow.DefaultCellStyle.BackColor = Color.FromArgb(236, 236, 255)
                    'Else

                    dgvRow.DefaultCellStyle.BackColor = Color.FloralWhite
                    dgvRow.DefaultCellStyle.ForeColor = Color.MediumOrchid
                    Me.DataGridView1.Rows(x).Cells(9).Value = False
                    xReportStructure.IncludeFolderinSchedule = True
                    RunReport.IASchReport(x) = xReportStructure

                    'End If

                    'http://condor.depaul.edu/sjost/it236/documents/colorNames.htm

                Else
                    'Me.DataGridView1.Rows(x).Frozen = True

                    'If dgvRow.Cells("Active?").Value <> True Then
                    dgvRow.DefaultCellStyle.BackColor = Color.LightGray
                    dgvRow.DefaultCellStyle.ForeColor = Color.DarkGray
                    Me.DataGridView1.Rows(x).Cells(9).Value = True
                    xReportStructure.IncludeFolderinSchedule = False
                    RunReport.IASchReport(x) = xReportStructure

                    'Else
                    'dgvRow.DefaultCellStyle.BackColor = Color.Red

                    'End If

                End If
            End If
        Next

        Me.DataGridView1.Focus()


    End Sub

    Private Sub ChkListBoxSystems_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ChkListBoxSystems.SelectedIndexChanged

    End Sub

    Private Sub ChkListBoxUsers_ItemCheck(sender As Object, e As System.Windows.Forms.ItemCheckEventArgs) Handles ChkListBoxUsers.ItemCheck

        Dim CheckUser As String = Me.ChkListBoxUsers.Items(e.Index).ToString
        'Dim Pos As Integer = InStrRev(CheckUser, " - ")
        'CheckUser = Mid(CheckUser, Pos + 3, 80)

        For x As Integer = 0 To IASchReport.Count - 1
            If UCase(IASchReport(x).UserCode) = UCase(CheckUser) Then
                Dim dgvRow As DataGridViewRow = Me.DataGridView1.Rows(x)
                Dim xReportStructure As Schreport = RunReport.IASchReport(x)
                If e.NewValue = CheckState.Checked Then

                    'Me.DataGridView1.Rows(x).Frozen = False

                    'If dgvRow.Cells("Active?").Value <> True Then
                    ' dgvRow.DefaultCellStyle.BackColor = Color.FromArgb(236, 236, 255)
                    'Else

                    dgvRow.DefaultCellStyle.BackColor = Color.FloralWhite
                    dgvRow.DefaultCellStyle.ForeColor = Color.MediumOrchid
                    Me.DataGridView1.Rows(x).Cells(9).Value = False
                    xReportStructure.IncludeFolderinSchedule = True
                    RunReport.IASchReport(x) = xReportStructure

                    'End If

                    'http://condor.depaul.edu/sjost/it236/documents/colorNames.htm

                Else
                    'Me.DataGridView1.Rows(x).Frozen = True

                    'If dgvRow.Cells("Active?").Value <> True Then
                    dgvRow.DefaultCellStyle.BackColor = Color.LightGray
                    dgvRow.DefaultCellStyle.ForeColor = Color.DarkGray
                    Me.DataGridView1.Rows(x).Cells(9).Value = True
                    xReportStructure.IncludeFolderinSchedule = False
                    RunReport.IASchReport(x) = xReportStructure

                    'Else
                    'dgvRow.DefaultCellStyle.BackColor = Color.Red

                    'End If

                End If
            End If
        Next

        Me.DataGridView1.Focus()

    End Sub



    Private Sub DataGridView1_CellValueChanged(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellValueChanged
        'PWR 10.04.2017 Changes to the grid now saving back to the Report.
        Dim ReportSchedule As Schreport = IASchReport(e.RowIndex)

        If IsPeriodValueCell(e) Then
            ReportSchedule.PeriodValue = Integer.Parse(DataGridView1.Rows(e.RowIndex) _
            .Cells("Period Value").Value.ToString())
            SaveScheduleReport(ReportSchedule, e.RowIndex)
        ElseIf IsPeriodTypeCell(e) Then
            ReportSchedule.Period = DataGridView1.CurrentCell.EditedFormattedValue
            SaveScheduleReport(ReportSchedule, e.RowIndex)
        ElseIf IsFromTimeCell(e) Then
            ReportSchedule.RunFromTime = TimeSpan.Parse(DataGridView1.Rows(e.RowIndex) _
            .Cells("From Time").Value.ToString())
            DataGridView1.Rows(e.RowIndex).Cells("From Time").Value = ReportSchedule.RunFromTime.ToString
            SaveScheduleReport(ReportSchedule, e.RowIndex)
        ElseIf IsTillTimeCell(e) Then
            ReportSchedule.RunTillTime = TimeSpan.Parse(DataGridView1.Rows(e.RowIndex) _
            .Cells("Till Time").Value.ToString())
            DataGridView1.Rows(e.RowIndex).Cells("Till Time").Value = ReportSchedule.RunTillTime.ToString
            SaveScheduleReport(ReportSchedule, e.RowIndex)


        End If

    End Sub

    Private Sub DataGridView1_CurrentCellDirtyStateChanged(sender As Object, e As System.EventArgs) Handles DataGridView1.CurrentCellDirtyStateChanged

        If DataGridView1.IsCurrentCellDirty Then
            DataGridView1.CommitEdit(DataGridViewDataErrorContexts.Commit)
        End If

    End Sub

    Private Sub FrmMain_Invalidated(sender As Object, e As InvalidateEventArgs) Handles Me.Invalidated

    End Sub

    Private Sub StartToolStripMenuItem_Click(sender As Object, e As EventArgs)
        StartFunctions()
    End Sub

    Private Sub PauseToolStripMenuItem_Click(sender As Object, e As EventArgs)
        PauseFunctions()
    End Sub

    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs)
        End
    End Sub

    Private Sub ToolStripButtonExit_Click(sender As Object, e As EventArgs) Handles ToolStripButtonExit.Click
        End
    End Sub

    Private Sub ToolStripButtonPause_Click(sender As Object, e As EventArgs) Handles ToolStripButtonPause.Click
        PauseFunctions()
    End Sub

    Private Sub ToolStripButtonPlay_Click(sender As Object, e As EventArgs) Handles ToolStripButtonPlay.Click
        StartFunctions()
    End Sub

    Private Sub ToolStripButtonRefresh_Click(sender As Object, e As EventArgs) Handles ToolStripButtonRefresh.Click
        RestartProgram()
    End Sub

    Private Sub ToolStripButtonDelete_Click(sender As Object, e As EventArgs) Handles ToolStripButtonDelete.Click
        DeleteSelectedReport()
    End Sub
End Class
