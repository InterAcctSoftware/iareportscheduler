﻿Imports System.Runtime.Versioning

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMain))
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.lblLastChecked = New System.Windows.Forms.Label()
        Me.lblNextScheduled = New System.Windows.Forms.Label()
        Me.lblLastRunReport = New System.Windows.Forms.Label()
        Me.ChkListBoxSystems = New System.Windows.Forms.CheckedListBox()
        Me.ChkListBoxUsers = New System.Windows.Forms.CheckedListBox()
        Me.lblSystems = New System.Windows.Forms.Label()
        Me.lblUsers = New System.Windows.Forms.Label()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.GroupBoxStatus = New System.Windows.Forms.GroupBox()
        Me.ToolStripButtonPlay = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButtonPause = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButtonRefresh = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButtonExit = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButtonDelete = New System.Windows.Forms.ToolStripButton()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.GroupBoxStatus.SuspendLayout()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(0, 165)
        Me.DataGridView1.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.DataGridView1.RowHeadersWidth = 62
        Me.DataGridView1.ShowEditingIcon = False
        Me.DataGridView1.Size = New System.Drawing.Size(1713, 320)
        Me.DataGridView1.TabIndex = 1
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'lblLastChecked
        '
        Me.lblLastChecked.AutoSize = True
        Me.lblLastChecked.Location = New System.Drawing.Point(7, 74)
        Me.lblLastChecked.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblLastChecked.Name = "lblLastChecked"
        Me.lblLastChecked.Size = New System.Drawing.Size(112, 20)
        Me.lblLastChecked.TabIndex = 8
        Me.lblLastChecked.Text = "Last checked :"
        '
        'lblNextScheduled
        '
        Me.lblNextScheduled.AutoSize = True
        Me.lblNextScheduled.Location = New System.Drawing.Point(7, 50)
        Me.lblNextScheduled.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblNextScheduled.Name = "lblNextScheduled"
        Me.lblNextScheduled.Size = New System.Drawing.Size(147, 20)
        Me.lblNextScheduled.TabIndex = 7
        Me.lblNextScheduled.Text = "Next Report to run :"
        '
        'lblLastRunReport
        '
        Me.lblLastRunReport.AutoSize = True
        Me.lblLastRunReport.Location = New System.Drawing.Point(7, 25)
        Me.lblLastRunReport.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblLastRunReport.Name = "lblLastRunReport"
        Me.lblLastRunReport.Size = New System.Drawing.Size(132, 20)
        Me.lblLastRunReport.TabIndex = 6
        Me.lblLastRunReport.Text = "Last Report ran : "
        '
        'ChkListBoxSystems
        '
        Me.ChkListBoxSystems.CheckOnClick = True
        Me.ChkListBoxSystems.FormattingEnabled = True
        Me.ChkListBoxSystems.Location = New System.Drawing.Point(13, 82)
        Me.ChkListBoxSystems.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.ChkListBoxSystems.Name = "ChkListBoxSystems"
        Me.ChkListBoxSystems.Size = New System.Drawing.Size(516, 73)
        Me.ChkListBoxSystems.TabIndex = 9
        '
        'ChkListBoxUsers
        '
        Me.ChkListBoxUsers.CheckOnClick = True
        Me.ChkListBoxUsers.FormattingEnabled = True
        Me.ChkListBoxUsers.Location = New System.Drawing.Point(540, 82)
        Me.ChkListBoxUsers.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.ChkListBoxUsers.Name = "ChkListBoxUsers"
        Me.ChkListBoxUsers.Size = New System.Drawing.Size(180, 73)
        Me.ChkListBoxUsers.TabIndex = 10
        '
        'lblSystems
        '
        Me.lblSystems.AutoSize = True
        Me.lblSystems.Location = New System.Drawing.Point(13, 57)
        Me.lblSystems.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblSystems.Name = "lblSystems"
        Me.lblSystems.Size = New System.Drawing.Size(139, 20)
        Me.lblSystems.TabIndex = 11
        Me.lblSystems.Text = "InterAcct Systems"
        '
        'lblUsers
        '
        Me.lblUsers.AutoSize = True
        Me.lblUsers.Location = New System.Drawing.Point(536, 57)
        Me.lblUsers.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblUsers.Name = "lblUsers"
        Me.lblUsers.Size = New System.Drawing.Size(51, 20)
        Me.lblUsers.TabIndex = 12
        Me.lblUsers.Text = "Users"
        '
        'ToolStrip1
        '
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButtonPlay, Me.ToolStripButtonPause, Me.ToolStripButtonRefresh, Me.ToolStripButtonDelete, Me.ToolStripButtonExit})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1713, 41)
        Me.ToolStrip1.TabIndex = 14
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'GroupBoxStatus
        '
        Me.GroupBoxStatus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBoxStatus.Controls.Add(Me.lblLastRunReport)
        Me.GroupBoxStatus.Controls.Add(Me.lblNextScheduled)
        Me.GroupBoxStatus.Controls.Add(Me.lblLastChecked)
        Me.GroupBoxStatus.Location = New System.Drawing.Point(727, 57)
        Me.GroupBoxStatus.Name = "GroupBoxStatus"
        Me.GroupBoxStatus.Size = New System.Drawing.Size(974, 97)
        Me.GroupBoxStatus.TabIndex = 15
        Me.GroupBoxStatus.TabStop = False
        Me.GroupBoxStatus.Text = "Current Status"
        '
        'ToolStripButtonPlay
        '
        Me.ToolStripButtonPlay.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButtonPlay.Image = Global.IAReportScheduler.My.Resources.Resources.play_64
        Me.ToolStripButtonPlay.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButtonPlay.Name = "ToolStripButtonPlay"
        Me.ToolStripButtonPlay.Size = New System.Drawing.Size(36, 36)
        Me.ToolStripButtonPlay.Text = "ToolStripButton1"
        Me.ToolStripButtonPlay.ToolTipText = "Start the Report Schedule"
        '
        'ToolStripButtonPause
        '
        Me.ToolStripButtonPause.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButtonPause.Image = Global.IAReportScheduler.My.Resources.Resources.pause_64
        Me.ToolStripButtonPause.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButtonPause.Name = "ToolStripButtonPause"
        Me.ToolStripButtonPause.Size = New System.Drawing.Size(36, 36)
        Me.ToolStripButtonPause.Text = "ToolStripButton1"
        Me.ToolStripButtonPause.ToolTipText = "Pause the Schedule"
        '
        'ToolStripButtonRefresh
        '
        Me.ToolStripButtonRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButtonRefresh.Image = Global.IAReportScheduler.My.Resources.Resources.refresh_64
        Me.ToolStripButtonRefresh.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButtonRefresh.Name = "ToolStripButtonRefresh"
        Me.ToolStripButtonRefresh.Size = New System.Drawing.Size(36, 36)
        Me.ToolStripButtonRefresh.Text = "Refresh List of Scheduled Reports"
        '
        'ToolStripButtonExit
        '
        Me.ToolStripButtonExit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButtonExit.Image = Global.IAReportScheduler.My.Resources.Resources.door_64
        Me.ToolStripButtonExit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButtonExit.Name = "ToolStripButtonExit"
        Me.ToolStripButtonExit.Size = New System.Drawing.Size(36, 36)
        Me.ToolStripButtonExit.Text = "ToolStripButton2"
        Me.ToolStripButtonExit.ToolTipText = "Exit the Program"
        '
        'ToolStripButtonDelete
        '
        Me.ToolStripButtonDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButtonDelete.Image = Global.IAReportScheduler.My.Resources.Resources.delete_64
        Me.ToolStripButtonDelete.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButtonDelete.Name = "ToolStripButtonDelete"
        Me.ToolStripButtonDelete.Size = New System.Drawing.Size(36, 36)
        Me.ToolStripButtonDelete.Text = "Delete Selected Report"
        '
        'FrmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1713, 485)
        Me.Controls.Add(Me.GroupBoxStatus)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.lblUsers)
        Me.Controls.Add(Me.lblSystems)
        Me.Controls.Add(Me.ChkListBoxUsers)
        Me.Controls.Add(Me.ChkListBoxSystems)
        Me.Controls.Add(Me.DataGridView1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "FrmMain"
        Me.Text = "IA Report Scheduler"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.GroupBoxStatus.ResumeLayout(False)
        Me.GroupBoxStatus.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents lblLastChecked As System.Windows.Forms.Label
    Friend WithEvents lblNextScheduled As System.Windows.Forms.Label
    Friend WithEvents lblLastRunReport As System.Windows.Forms.Label
    Friend WithEvents ChkListBoxSystems As System.Windows.Forms.CheckedListBox
    Friend WithEvents ChkListBoxUsers As System.Windows.Forms.CheckedListBox
    Friend WithEvents lblSystems As System.Windows.Forms.Label
    Friend WithEvents lblUsers As System.Windows.Forms.Label
    Friend WithEvents ToolStrip1 As ToolStrip
    Friend WithEvents ToolStripButtonPlay As ToolStripButton
    Friend WithEvents ToolStripButtonPause As ToolStripButton
    Friend WithEvents ToolStripButtonExit As ToolStripButton
    Friend WithEvents GroupBoxStatus As GroupBox
    Friend WithEvents ToolStripButtonRefresh As ToolStripButton
    Friend WithEvents ToolStripButtonDelete As ToolStripButton
End Class
